import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

//Global styles
import './components/Global/Reset.css';
import './components/Global/Variables.css';
import './components/Global/Text.css';
import './App.css';

//Components
import List from './components/List/List';

//Pages
function Orders() {
	return <List title='Orders' data={orders} allowAdd={false} allowRemove={false} allowEdit={false} />
}

function Customers() {
	return <List title='Customers' data={customers} allowAdd={true} allowRemove={true} allowEdit={true} />
}

// Data cop
let orders= [
	{
		id: 0,
		name: 'Saprom',
		value: 'So la canto do possoma. Vol navio olaponte'
	},
	{
		id: 1,
		name: 'Saprom monte',
		value: 'Secallo fasse'
	},
	{
		id: 2,
		name: 'Numm',
		value: 'Dulla numo'
	}
]
let customers= [
	{
		id: 0,
		name: 'Malio',
		value: 'Soma peolosa ver drat'
	},
	{
		id: 11,
		name: 'Dofa spacido',
		value: 'Nopo cona estaesa'
	},
	{
		id: 2,
		name: 'Leru manos',
		value: 'Sal per vodos nofae la cerma'
	},
	{
		id: 3,
		name: 'Unnam as pattitos',
		value: 'Keno bra lommnasse ver pattome'
	}
]


function App() {
	return (
		<div className='App'>
			<Router>
				<nav className='App-nav'>
					<Link to='/orders/' className='App-link'>Orders</Link>
					<Link to='/customers/' className='App-link'>Customers</Link>
				</nav>
				<Route path='/orders/' component={Orders}/>
				<Route path='/customers/' component={Customers}/>
			</Router>
		</div>
	)
}

export default App;
