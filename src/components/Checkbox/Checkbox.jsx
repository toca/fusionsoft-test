import React from 'react';
import './Checkbox.css';

class Checkbox extends React.Component {
	
	// constructor(props) {
	// 	super(props);
	// 	this.state = {
	// 		checked: this.props.checked || false
	// 	}
	// 	this.handleToggle = this.handleToggle.bind(this);
	// }
	
	// handleToggle(e) {
	// 	this.setState({checked: e.target.checked});
	// }
	
	render() {
		return(
			<label className='Checkbox' >
				<input className='Checkbox-input' type='checkbox' checked={this.props.checked} onChange={this.props.onChange}/>
				<span className='Checkbox-icon'/>
			</label>
		)
	}
}

export default Checkbox;
