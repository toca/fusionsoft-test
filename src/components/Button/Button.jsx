import React from 'react';
import './Button.css';

class Button extends React.Component {
	render() {
		return(
			<button 
				onClick={this.props.onClick} 
				className={
					'Button' + (this.props.warning ? ' asWarning' : '' ) +
						(this.props.main ? ' asMain' : '') +
						(this.props.hidden ? ' isHidden' : '') + 
						(this.props.disabled ? ' isDisabled' : '')
				}>
				{this.props.label}
			</button>
		);
	}
}

export default Button;
