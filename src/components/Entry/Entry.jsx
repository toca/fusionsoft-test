import React from 'react';
import Button from '../Button/Button';
import './Entry.css';

class Entry extends React.Component {
	constructor(props) {
		super(props);
		
		this.state = {
			isExpand: (props.data.name.toString() === '') ? true : false,
			isEdit: (props.data.name.toString() === '') ? true : false,
			willCollapse: false,
			allowSave: true,
			name: props.data.name,
			value: props.data.value,
			inputName: props.data.name,
			inputValue: props.data.value
		}
		
		this.inputName = React.createRef();
		
		this.edit = this.edit.bind(this);
		this.toggle = this.toggle.bind(this);
		this.cancel = this.cancel.bind(this);
		this.save = this.save.bind(this);
		this.handleName = this.handleName.bind(this);
		this.handleValue = this.handleValue.bind(this);
	}
	
	componentDidMount() {
		this.isAllowSave(this.state.name);
		if (this.props.data.name === '') {
			this.inputName.current.focus();
		}
	}
	
	edit(e) {
		e.preventDefault();
		this.setState(state => ({
			willCollapse: !state.isExpand,
			isExpand: true,
			isEdit: true,
		}));
		this.inputName.current.focus();
	}
	
	toggle() {
		this.setState(state => ({
			isExpand: state.isEdit ? true : !state.isExpand
		}));
	}
	
	cancel() {
		this.setState(state => ({
			isEdit: false,
			isExpand: !state.willCollapse,
			inputName: state.name,
			inputValue: state.value
		}));
		if (this.props.onCancel) this.props.onCancel();
	}
	
	save(e) {
		e.preventDefault();
		this.setState(state => ({
			isEdit: false,
			isExpand: !state.willCollapse,
			name: state.inputName,
			value: state.inputValue
		}));
		if (this.props.onSave) this.props.onSave({
			id: this.props.data.id,
			name: this.state.inputName,
			value: this.state.inputValue
		});
	}
	
	isAllowSave(value) {
		if (value.toString() !== '') {
			this.setState({
				allowSave: true
			});
		} else {
			this.setState({
				allowSave: false
			});
		}
	}
	
	handleName(e) {
		this.setState({inputName: e.target.value});
		this.isAllowSave(e.target.value);
	}
	
	handleValue(e) {
		this.setState({inputValue: e.target.value});
	}
	
	render() {
		
		let bar;
		if (this.props.editable) {
			bar = (
				<div className='Entry-bar'>
					<div className='Entry-control'>
						<Button label='Save' onClick={this.save} disabled={!this.state.allowSave}/>
						<Button label='Cancel' onClick={this.cancel}/>
					</div>
					<div className='Entry-action'>
						<Button label='Edit' onClick={this.edit}/>
					</div>
				</div>
			)
		}
		
		return(
			<div className={ 'Entry' + (this.state.isExpand ? ' expand': '') + (this.state.isEdit ? ' edit' : '') }>
				<form className='Entry-form' onSubmit={this.save} >
					<div className='Entry-bar'>
						<span className='Entry-name' onClick={this.toggle}>
							{this.state.name}
						</span>
						<input 
							className='Entry-input' 
							size='1' 
							value={this.state.inputName} 
							onChange={this.handleName} 
							ref={this.inputName}
						/>
						{bar}
					</div>
					<div className='Entry-content' >
						<span className='Entry-value'>
							{this.state.value || 'Empty'}
						</span>
						<input 
							className='Entry-input' 
							size='1' 
							value={this.state.inputValue} 
							onChange={this.handleValue} 
						/>
					</div>
				</form>
			</div>
		);
	}
}

export default Entry;
