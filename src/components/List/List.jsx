import React from 'react';
import Button from '../Button/Button';
import Checkbox from '../Checkbox/Checkbox';
import Entry from '../Entry/Entry';
import './List.css';

function setSelection(target, value) {
	let selected = {};
	for (let key in target) {
		selected[target[key].id] = value;
	}
	return selected;
}

class List extends React.Component {
	
	constructor(props) {
		super(props);
		
		this.state = {
			data: this.props.data,
			selected: setSelection(this.props.data, false),
			activeDeselect: false,
			activeSelectAll: true,
			activeDelete: false,
			isAdd: false
		}
		
		this.handleSelect = this.handleSelect.bind(this);
		this.deselect = this.deselect.bind(this);
		this.selectAll = this.selectAll.bind(this);
		this.delete = this.delete.bind(this);
		this.create = this.create.bind(this);
		this.save = this.save.bind(this);
		this.cancel = this.cancel.bind(this);
	}
	
	handleSelect(id) {
		let selected = this.state.selected;
		selected[id] = !selected[id]
		
		let isSelected = Object.keys(selected).some( key => (selected[key] === true));
		let allSelected = !Object.keys(selected).every( key => (selected[key] === true));
		
		this.setState({
			selected: selected,
			activeDeselect: isSelected,
			activeSelectAll: allSelected
		});
	}
	
	deselect() {
		this.setState({
			selected: setSelection(this.state.data, false),
			activeDeselect: false,
			activeSelectAll: true
		});
	}
	
	selectAll() {
		this.setState({
			selected: setSelection(this.state.data, true),
			activeDeselect: true,
			activeSelectAll: false
		});
	}
	
	delete() {
		let selected = this.state.selected;
		let result = this.state.data.filter( (el) => !( 
			Object.keys(selected).some(() => (selected[el.id])) 
		));
		this.setState({
			data: result,
			selected: {},
			activeDeselect: false,
			activeSelectAll: (result.length > 0)
		});
	}
	
	create() {
		let newID = +new Date();
		// let result = this.state.data;
		let result = this.state.data.filter( (el) => true); 
		result.push({id: newID, name: '', value: ''});
		this.setState({
			data: result,
			activeSelectAll: (result.length > 0)
		});
	}
	
	save(data) {
		let result = this.state.data;
		let index = result.findIndex( (obj) => obj.id === data.id);
		result[index].name = data.name;
		result[index].value = data.value;
		this.setState({
			data: result,
		});
	}
	
	cancel(id) {
		let result = this.state.data.filter( (el) => (
			this.state.data.some( () => !((el.id === id) && (el.name === '')) )
		));
		this.setState({
			data: result
		});
	}
	
	render() {
		
		let create;
		if (this.props.allowAdd) {
			create = <Button label='Create New' main={true} onClick={this.create}/>
		}
		
		let control;
		if (this.props.allowRemove) {
			control = (
				<div className='List-bar'>
					<div className='List-control'>
						<Button label='Select All' hidden={!this.state.activeSelectAll} onClick={this.selectAll}/>
						<Button label='Deselect' hidden={!this.state.activeDeselect} onClick={this.deselect}/>
					</div>
					<div className='List-separator'></div>
					<div className='List-control'>
						<Button label='Delete' warning='true' hidden={!this.state.activeDeselect} onClick={this.delete}/>
					</div>
				</div>
			)
		}
		
		return(
			<div className='List'>
				<div className='List-row'>
					<div className='List-title'>{this.props.title}</div>
					{create}
				</div>
				<div className='List-data'>
					{control}
					<div className='List-content'>
						{
							this.state.data.map( (item) => {
								return(
									<div className='List-item' key={item.id}>
										{ this.props.allowRemove && <Checkbox onChange={this.handleSelect.bind(this, item.id)} checked={!!this.state.selected[item.id]}/> }
										<Entry 
											data={item} 
											editable={this.props.allowEdit} 
											onSave={this.save}
											onCancel={this.cancel.bind(this, item.id)}
										/>
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		);
	}
}

export default List;
